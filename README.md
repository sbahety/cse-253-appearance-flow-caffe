# [View Synthesis by Appearance Flow](https://arxiv.org/abs/1605.03557)
## Overview

We address the problem of novel view synthesis: given an input image, synthesizing new images of the same object or scene observed from arbitrary viewpoints. We approach this as a learning task but, critically, instead of learning to synthesize pixels from scratch, we learn to copy them from the input image. Our approach exploits the observation that the visual appearance of different views of the same instance is highly correlated, and such correlation could be explicitly learned by training a convolutional neural network (CNN) to predict **appearance flows** – 2-D coordinate vectors specifying which pixels in the input view could be used to reconstruct the target view. Furthermore, the proposed framework easily generalizes to multiple input views by learning how to optimally combine single-view predictions. 

Link to the [[Paper]](https://arxiv.org/abs/1605.03557) [[Poster]](https://people.eecs.berkeley.edu/~tinghuiz/projects/appearanceFlow/poster.pdf)

## Installation Instructions


* Create Ubuntu 16.04 instance on amazon-ec2
* Clone the repository using ```git clone https://sbahety@bitbucket.org/sbahety/cse-253-appearance-flow.git```
* Do the following in the ec2-instance (Copied from [Here](https://github.com/BVLC/caffe/wiki/Ubuntu-16.04-or-15.10-Installation-Guide)):
* ```sudo apt-get update```
* ```sudo apt-get upgrade```
* ```sudo apt-get install -y build-essential cmake git pkg-config```
* ```sudo apt-get install -y libprotobuf-dev libleveldb-dev libsnappy-dev libhdf5-serial-dev protobuf-compiler```
* ```sudo apt-get install -y libatlas-base-dev ```
* ```sudo apt-get install -y --no-install-recommends libboost-all-dev```
* ```sudo apt-get install -y libgflags-dev libgoogle-glog-dev liblmdb-dev```
* ```sudo apt-get install -y python-pip```
* ```sudo apt-get install -y python-dev```
* ```sudo apt-get install -y python-numpy python-scipy```
* ```sudo apt-get install -y python3-dev```
* ```sudo apt-get install -y python3-numpy python3-scipy```
* ```sudo apt-get install -y libopencv-dev```
* ```find . -type f -exec sed -i -e 's^"hdf5.h"^"hdf5/serial/hdf5.h"^g' -e 's^"hdf5_hl.h"^"hdf5/serial/hdf5_hl.h"^g' '{}' \;```
* ```cd /usr/lib/x86_64-linux-gnu```
* ```sudo rm libhdf5.so libhdf5_hl.so```
For 16.04:
* ```sudo ln -s libhdf5_serial.so.10.1.0 libhdf5.so```
* ```sudo ln -s libhdf5_serial_hl.so.10.0.2 libhdf5_hl.so```

* Go to ```./caffe/build``` folder (Create ```build``` folder if not present.)
* Run ```cmake ..```
* Run ```make all -j8```
* Run ```make install```


## Running the code

* First do Installation, if not done
* Copy chair.tar.gz from drive folder to ```./data/chair/```
* Extract tar file in the ```chair``` folder using ```mv chair.tar.gz chair.tar; tar xvf chair.tar```
* Goto ```./data/chair/``` folder
* Run ```find images/ -iname *.png > trainlist.txt; python createdata.py trainlist.txt traindata.txt```
* Goto root directory of the repo
* Run ```GLOG_logtostderr=1 ./caffe/build/tools/convert_imageset --resize_height=224 --resize_width=224 --shuffle ./data/chair/ ./data/chair/traindata.txt ./data/chair/train_lmdb```. Check to see if ```train_lmdb``` is created in ```./data/chair```
* Run the classififer using ```caffe/build/tools/caffe train --solver=models/chair_single/solver.prototxt```
* If there's opencv error, try this ```sudo ln /dev/null /dev/raw1394```